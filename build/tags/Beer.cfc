<!--- 
 * Copyright (c) 2015, Paul Klinkenberg. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either 
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public 
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 * 
 ---><cfcomponent output="no">
	<cffunction name="init" output="no" returntype="void" hint="invoked after tag is constructed">
		<cfargument name="hasEndTag" type="boolean" required="yes">
		<cfargument name="parent" type="component" required="no" hint="the parent cfc custom tag, if there is one">
	</cffunction>
	
	<cffunction name="onStartTag" returntype="boolean">
		<cfargument name="attributes" type="struct">
		<cfargument name="caller" type="struct">
		<!--- get google map with local bars --->
		<cfsavecontent variable="headText"><cfoutput>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true&libraries=places"></script>
			<script type="text/javascript">
				var map = '';
				var infowindow;
				var geoloaded = 0;
				var noGeoPauses = 0;
				var middlepoint;
				var searchradius = 1000;
				var mugB64="iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADaRJREFUeNqsWAlYVWUafu/C5W5w2S8gIBdXFAHBRBFTEHHCsUVm2rSasmzX5rGmmWlKp+dpm2amLCtzLHsaqwmz3FJLBFEIBRdAURZZleWyXu6+3/n+cwAhFWw5POfh3PNv7//97/d+33cEHo8H17tcLhd3SySSoXd5eXmxubm5b6rV6kwaa2tsbDxHzxp67hKLxbuXL1/+wdy5c9vxK12CawFsbW1VbN++/Ql6THM6nR6BQHAhLS3tE4PBIC4tLc1bt25dqK+vL9dXr9eDPbN56urq8Omnn7bFxcWtyMjIKKyqqtL4+fkZYmNju6RS6S8DaDabBQcPHkwxGo3Bhw4dylm7du0Ds2bN4hbu6enBtm3bdCUlJX07duzQiESiUSddtWpVa1BQUH98fPxknU5nO3fu3BePPvrousTERP3PAtjc3Cx8++23XydAz8XExMBisYAsMKKjzWZDZ2cn5HI5/P39IRQKrzupVqvl+gxSo6ysDLSxnkmTJq165JFHdv8kgHSUGSdPnvxPenq65tZbbxVcr+PFixexZ88eDqhGo8Hdd9/9k49rw4YNHbfddlvCzJkzO290jIh2+vVbb70VO3369GuCY9b87rvvmAWwZs0aLFiwAN7e3ggODh7qU19fz7UfPXoUlZWVIA6CHOaqucj6yo8//riT2rpDQkL0NI97TIC0o42LFi26ajY6dhAnceDAARCfsGzZMoSHh3MLDwdHjoAvv/wSd9xxB7Kzs1FbW4uioiKEhoaCHGTEnCqVClarNYuo8nRBQYHl5ptvLhoTIB3X0pSUlAilUjn0cu/evfjkk08wZcoUkGSAJuI4da2LnIrrExkZOWglzrMLCws5yyoUCg4o4yzj5IwZM5CcnIzi4uKJtNGttHn7aADFZOYjbW1tc9iO+/v7sXnzZkREROCNN96Aj4/PmBxhTjX8mjBhAnf39vYyEDh27BjTTjz++OMj+gUEBIS8887GP0ycMD5AJBJE2OxOo49PQNmiRYv3T548sX8IIFkui9yf+8GsRs6C2bNn/2KBJQAcLdi1ceNGtLe3IywsDEeOFOJMeTmM+nbZPb9LfTcmJpqzssPpIIs348MPXmuOi0+778EHHzzGAaRLycxvMpnA9O3XADf8YjLG5h48jdOnyxAVLkb2ww9BrlTRGwPgptspQnBYHOakJY3/y59e37X/QFhq9i2/qRGR9WYQ+ZOjoqI47/Py8rqhhc1mC+cwFGVG1zFqb2lp4Y66vLwCcqkVKx+4H2KRCdWVRTh14jj0fT0ICFTQfC7AbsKs2fGyLVu+UC9cmLVDtHr1ahPx5T5SfcGPwVGYg8Ph4ICYzVZuMWZtZpW1z/yFrH0THY9szM10d3eT99qg67mAp59ZjbLiA/hgc25N02W86hsQ+2pVTW/Rzp2HAnwUgvGRmjBIFVJo29omtraZvxUtXrw4noh+LxPfH19sx+tfeh6BwRF4952XUVvXAJdbgo+2fgCF8D+orTmNE8cPo/iHU0hNTb8uQDY3SSCaG8+g6OjRzgt19j8/vPrZ1dlLlxZRnG5OSZlz5qbZ6dvefe9zeVS4bF5IaCB85WJxXkGFVkiWq6BIclX2kZu7Hbu/3oSVOX1w9a9HztJe+HnlQdizGkrnv7Dinl48uXIvVmZ/BKe5ADb76FY8e7Yce/ad/Cpjyaq4F1/asCkyMtw60ql8sWLlqn8d/P5UN4g1kRFqWMzd84Tz589v7urq+p5p1xViAzXn8/HkKisysiZiQfZ4qMOCkBQvxtz0iwgPscJuBLzpdINI/sKDyijenh0VYGlpGbKW3PLNzMSEruv1mREf12k0CSpcRhMkCjmUckwVMl6RkDqGZyiM9yGh8dDpHJQUOilTcEPb3g1T52HyDgPcbn4ToNtjBnKWWfHp1t/jXFXDdQHm5OSwf36jbUIhl8LpFjXoDSZAKIKPUuLPpSSUq51iIWr4ZbV5SLjpFISEQmhBR2sRpT4WPvzQKNdAFPVwiS2gVPpdFdpGLE6SYrM7AsdyqMCAYANzKHYpCTEHMDMzcweJdH1fX99QR7fbCT5XJHPaT8Ni6qEdsQZA4sXSL75JSHnogTwhluW8h4hxAUOZuNPpGkg27NB29uPlF+6CUVc3ZmjS9eu8vSS8mtgdDicHMCEhoef222//7fr169//8MMP+9vaulF+eh/JSATc5ibAUU0yAcYJzmReYjaYB8h+6wzB0MRMHFrk2LGjeO7ZJ1B2qgZ/fGYF3tv0KnIyKhESoPcfDZzD6YZQ4IxSMunyuKE32vqHsk4KcdWUrz1JzlJ9/nwl1EFkGjet7izn/pMcQiq5YkG7fQAgXWHBWpyrPDG0UEd7PeZO24JLZ6dh3YNfwcv6D2TMJWua9P6uURKsxoYmpbe3M17q68MtQMl4s/hHWTPpsETRXF+EWQn+MBoaIBe1c0AYzzgd9/AAGWCOCnTUi0kCH3v2fioNXoPKLwpHDr2JTa9THJXxaAILiLdsM269H7O8zPvaAHfuzM2ZM0sznjlIy+VWeEkCCkcApJA0TiCURIsFlxEWKoXdfBZynyvSc62oxt6z7D9hihaxyodgoDwkOoQ2REAEhE8k4Q9CSGPFQoPKYnESwKuT2a0fbVsil7RvSs+4k7khdu8tssQnLtw+oielXVMpqCtVCj2cNjPleh0IUA04gwCcvPAONBIsa2O/Z8aTlYkZ5xp5i4tF/FiWXDOQBNCHPJR+iZ2Dm6ura5Bt/+/HT4QGGV9Zu/Yub4glOH3iFJpa3O8+tfb28hEAqcYNiYicBHO/FBbdSYQGDfBMyMkSkZj/7aDFB43AFmEABELGMV6V3ANW5dEzGaM2ooJEpFft27c/yVfpUl1qaUg26LtmSiW21GW/iY+4aV4SZ7mqM5XYuq1o14vr//kC2/QQwIaGBlVXV+e6tPlLUHakA/7eLcNSEl77nAMAmXYzT8ZgSU3gmc6zcKeQXys3pISBFMxHZghtaMw/vjh9qiA5LhLjwpMhZ5m6gJHahN3f5ONgQdPWv/7t9afCwkI4Kw8B7Ojo8FepAicXHP4at86pxfEyB5gzedwDRhxmQSZxI2oiIe9ADKBn0OiDFqS+ocTJy5eJm+PI4xMSBTdnLmDFAtxWEy7XN6HybD1+KG2sHheV9OK/33r+K5n0ihcNLZOamtrU2tY975vcV7asWW6a/b2ZqTrPN+aBzGKDAJlUcJHRc8XCDLDNMRACBzjLUYDGhIcRL0mt0ugUvz1VBi+KSJXnmrr7dI4qqTyoRBMTm/fkmlXFZDXrVTXJ8B8LFmZWNFW9KTYbejmySxVkrYEhomHiPARwePUl4h1j0NuZBZn12QaD6BSNVqaXQEnJ0fKQyOzHs5beUh0dHaVTyEfPJ0cArK2t04QH1Md29zrBFXHDQDCP5DiIYR7qGQmQax88YwH/yCwppbncAzxOjnMhc3H2CXWw1HMjmfuI7xd1NWeSp2m6ZV09rHykFzJW9tF/yYAjCIeXg8PeSa5shgHlZIjaxXQCJqLK/z6nZ3onJ1UI82+Iq6g4E3ujNc1IHbx0Im1ZPKX4tOjmL1nNC0yfwoO9eBFYlMCTnoXKslPErWD+SDvqqJ2SoexZBJDaFAT4/c3g5ukn751BYXr5ChZsgdmJEO86sTcjK3Pu+Z8EsLqmVdCjPR4fpOaP6Y8PUQVWAVQRua3kndnzADV5o5u0bsl8YH8h8Nnn/FgWuu6jCjOQkikXce3+OwByTlBpQTU2LcKiER2/k8bW0UYLDu960WT2hP3p+RdeorjgGvPr1oEDB5Pz8/bm2i1NMdPD92MxgYkeT42+g9UTy334xTmKkWVEUt6a3AvapoesQxka5xxM1uDNj6MsDXXNQGWtGI3aqYjQLMX9K7Ox79vDMLumZK1Yce+hUS1oJfMU5O/9699fXhsjlfmgML8YXxTugt1QjFD/FkyOciOK9EsdxBJIWlwyjLmDTuTh/YJqb/TQkbZqKa63C9HWGwybZyoC1UmYlpKEnDgNFH4SbkBASBB0zbYxS0JxW1urUKXynSCn6t5DhUZ6Zgrd89DX1Yfz1fW4WHcBpT9cgNXUCIGzHUqZDjKJiXJDO2mjm5YSwWyVwWL3g90TCqk8mhafiKjpEzAnOgKhahXxkoUhO2U+DnisbghIiL87WNZ6573PFY8JcNy4CHdfX39tj7YxIVBNhHGSwpMp/FQezEvTYN78CdRtKVyUGesNZioDjDBQzWClDJbVzd7eEvj4KKi/km4ZvGR03iIPFw89pOxul5mSXqqnKUcTUhzs12qx8ZWdpuiJ8x9LTJzRc0NfWEtKjk/47L/vb0lNiU5PmztdEBkVyu2Sc1HKDDwkfKyfgP0xknG34EpRQrriodtNWQIv0kJeKMW85ph6dai60IiS0hpTY4t5zy1L73p5SVZm9U/6Rm00mpGfn598tqL0NputZ2FwoHdsdFRQUFRECNTqACqIfCCVSfmYxsTPgyuqLBgASaHGYragu1uHS61daGrWOi5d7m8yW8WlgcGR38+ZMz8vKWlmm5eX6Jd95e/XG4l79cH19XVTOzouT+/r6ZhmsxliREJnpFwuDpR4CVQ+Srm3RCIW22x2p15vttJpGqgS1Lrc4maZ3O9CsDqiIkYz6eykyZProyLHUaou+Fkfn/4vwAA0jr9VMP3c/wAAAABJRU5ErkJggg==";
						
				function initmap()
				{
					// Try HTML5 geolocation
					if(navigator.geolocation) {
						var noGeoTimeout = setTimeout(checkGeoIPLoaded, 5000);
						navigator.geolocation.getCurrentPosition(
							function(position) {
								if (map==='')
								{
									clearTimeout(noGeoTimeout);
									showmap(position.coords.latitude, position.coords.longitude);
								}
							}
							, function(){
								if (map==='')
								{
									clearTimeout(noGeoTimeout);
									checkGeoIPLoaded();
								}
							}
							, {maximumAge:600000, timeout:4000}
						);
					} else {
						checkGeoIPLoaded();
					}
				}
			
				function checkGeoIPLoaded()
				{
					if (geoloaded!==0)
					{
						showmap(geoloaded.lat, geoloaded.lng);
					} else
					{
						noGeoPauses+=10;
						if (noGeoPauses<=2000)
						{
							setTimeout(checkGeoIPLoaded, 10);
						} else
						{
							// minneapolis
							showmap(44.968491, -93.274398);
						}
					}
				}
				
				function geoiploaded(d)
				{
					geoloaded = {lat:parseFloat(d.latitude), lng:parseFloat(d.longitude)};
				}
				
				function showmap(lat, lng) {
					middlepoint = new google.maps.LatLng(lat, lng);
			
					map = new google.maps.Map(document.getElementById('map'), {
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: middlepoint,
						zoom: 14
					});
					
					doSearch();
				}
				
				function doSearch()
				{
					
					var request = {
						location: middlepoint,
						radius: searchradius,
						types: ['bar','cafe','restaurant']
					};
					infowindow = new google.maps.InfoWindow();
					var service = new google.maps.places.PlacesService(map);
					service.search(request, callback);
				}
			
				function callback(results, status) {
					if (status == google.maps.places.PlacesServiceStatus.OK) {
						if (results.length==0)
						{
							searchradius*=2;
							doSearch();
							return;
						}
						for (var i = 0; i < results.length; i++) {
							createMarker(results[i], i);
						}
					}
				}
			
				function createMarker(place, nr) {
					var placeLoc = place.geometry.location;
					var marker = new google.maps.Marker({
						  map: map
						, position: place.geometry.location
						,icon:"data:image/gif;base64,"+mugB64
					});

					google.maps.event.addListener(marker, 'click', function() {
						infowindow.setContent('Go drink a <span style="color:brown">&lt;cfbeer&gt;</span> at<br /><b>'+place.name + '</b><br /><i>('+place.vicinity+')</i>');
						infowindow.open(map, this);
					});
					
					if (nr==0)
					{
						setTimeout(function(){ new google.maps.event.trigger( marker, 'click' ); }, 2000);
					}
				}
				google.maps.event.addDomListener(window, 'load', initmap);
			</script>
			<script type="text/javascript" src="http://freegeoip.net/json/?callback=geoiploaded"></script>
			<style type="text/css">
				##map div div div div div div div div { overflow:hidden !important }
			</style>
		</cfoutput></cfsavecontent>
		<cfhtmlhead text="#headText#"/>
		
		<cfoutput>
			<div id="mapholder" style="width:99%;height:99%;border: 1px solid ##333; margin:10px 0; position:relative;">
				<div id="map" style="width:100%;height:100%; z-index:1;"><div style="text-align:center; margin-top:50px;"><em>Checking your geo location...</em></div></div>
			</div>
		</cfoutput>
		
		<cfreturn true>
	</cffunction>
	
	<cffunction name="onEndTag" output="yes" returntype="boolean">
		<cfargument name="attributes" type="struct">
		<cfargument name="caller" type="struct">
		<cfreturn false>
	</cffunction>
</cfcomponent>